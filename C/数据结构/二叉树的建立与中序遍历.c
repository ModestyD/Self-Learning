  #include <stdio.h>
  #include <stdlib.h>
  #include <memory.h>
  
  typedef struct treenode{
      char data;
      struct treenode * left;
      struct treenode * right;
      struct treenode * father;
 }bitnode;
 
void createTree(bitnode * pRoot)
 {
     char ch = 0;
     bitnode * pCur = pRoot;
     while((ch = getchar())!= 'e')
     {
         //printf("%c" , ch);
         bitnode * pNewNode = (bitnode *)malloc(sizeof(bitnode));
         pNewNode->left = NULL;
         pNewNode->right = NULL;
         pNewNode->father = NULL;
         if(ch == 'L')
         {
             //printf("Input L\n");
             pNewNode->data = getchar();
             pNewNode->father = pCur;
             pCur->left = pNewNode;
             pCur = pNewNode;
         }
         else if(ch == 'R')
         {
             //printf("Input R\n");
             pNewNode->data = getchar();
             pNewNode->father = pCur;
             pCur->right = pNewNode;
             pCur = pNewNode;
         }
         else if(ch == 'B')
         {
             //printf("Input B\n");
             free(pNewNode);
             if(pCur->father != NULL)
                 pCur = pCur->father;
             else
                 printf("It's the top\n");
         }
     }
 };
 
 void midorder(bitnode * pRoot)
 {
     bitnode * pCur = pRoot;
     if(pCur != NULL)
     {
         if(pCur->left != NULL)
         {
             midorder(pCur->left);
         }
         else
         {
             printf("%c ", pCur->data);
             return;
         }
         printf("%c ", pCur->data);
 
         if(pCur->right != NULL)
         {
             midorder(pCur->right);
         }
     }
 };
 
 
 int main()
{
     bitnode root;
     memset(&root,0,sizeof(bitnode));
     printf("请创建二叉树根节点与左子节点用L后跟字符表示，右子节点用R后跟字符表示，返回上一节点用B表示，结束创建用e表示: \n");
     createTree(&root);
     printf("中序遍历结果为: \n");
     midorder(&root);
     return 0;
 }