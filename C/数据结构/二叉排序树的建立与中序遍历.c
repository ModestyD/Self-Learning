#include<stdio.h>
#include<stdlib.h>
typedef struct treenode{
	int date;
	struct treenode *left;
	struct treenode *right;
};

typedef struct treenode bitNODE;

void creattreenode(bitNODE **R,int date)//定义递归建立有序二叉树函数 
{
	
	if(*R==NULL)
	{													  
        *R=(bitNODE*)malloc(sizeof(bitNODE));  
        (*R)->date=date;  
        (*R)->left=NULL;  
        (*R)->right=NULL;  
    }
    else
	{  
        if(date<(*R)->date)
		{  
            creattreenode(&((*R)->left),date);  
        }
		else if(date>(*R)->date)
		{  
            creattreenode(&((*R)->right),date);  
        }  
    }
};

void passdate(bitNODE **r,int a[],int q)//定义向建立有序递归二叉数函数传递节点值的函数 
{
	int i;  
    for(i=0;i<q;i++)
	{  
        creattreenode(&(*r),a[i]);  
    }
}
void midorder(bitNODE* T)//定义中序排序函数 
{
	if(T)
	{
		midorder(T->left);
		printf(" %d ",T->date);
		midorder(T->right);
	}
}
int main()
{
	bitNODE *Ta=NULL;
	int a[50];
	int i,j;
	int date;
	printf("请输入待存储新节点数值，以0结束： \n");  
    for(i=0;i<50;i++)
	{  
		printf("请输入第%d个节点数值:",i+1);
        scanf("%d",&a[i]);  
        if(a[i]==0)
		{  
            break;  
        }  
    }
    passdate(&Ta,a,i);
	printf("中序遍历结果为：\n");
	printf("\n");
	midorder(Ta);
	printf("\n");
	return 0;
}

