#include<stdio.h>
void halfsearch(int *a,int i,int n)//定义折半查找算法 
{
	int low=0,high=n,number=0;
	int mid;  
	while(low<=high)  
    {  
        mid=(low+high)/2;  
        if (i==a[mid])  
        {  
        	number++;
            printf("要找的数%d在第%d个位置(一共查找了%d次).\n",i,mid+1,number);  
            break;  
        }  
        else if(a[mid]>i)  
        {  
            high=mid-1;
			number++;  
        }  
        else  
        {  
            low=mid+1;
			number++;  
        }  
    }  
     
};

int main()
{
	int a[10]={1,23,72,88,99,100,101,152,168,200} ;
	int i;
	for(i=0;i<10;i++)
	{
		printf(" %d ",a[i]);
	}
	printf("\n");
	printf("请输入你要查找的数:");
	scanf("%d",&i);
	halfsearch(a,i,10);
	return 0;
} 