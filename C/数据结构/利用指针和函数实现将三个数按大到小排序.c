#include <stdio.h>
void exchange(int *pt1,int *pt2)//定义交换指针函数 
{
	int temp;
	temp=*pt1;
	*pt1=*pt2;
	*pt2=temp;
}
int main()
{
	int a,b,c,*p1,*p2,*p3;
	printf("请输入三个数字并用空格隔开：\n");
	scanf("%d%d%d",&a,&b,&c);
	p1=&a;
	p2=&b;
	p3=&c;
	if(*p1<*p2)//根据大小判断执行函数 
	{
		exchange(p1,p2);
	}
	if(*p1<*p3)
	{
		exchange(p1,p3);
	}
	if(*p2<*p3)
	{
		exchange(p2,p3);
	}
	printf("三个数字由大到小排序：%d %d %d\n",a,b,c);
	
	return 0;
}