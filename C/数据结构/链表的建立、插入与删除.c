#include <stdio.h>
#include <malloc.h>
struct node//定义链表结构体
{
	int date;
	struct node *next;
};

typedef struct node NODE;

void print(NODE *head)//定义打印函数
{
	NODE *p;
	p=head->next;
	printf("该链表为：");
	while(p->date!=0)
	{
		printf(" %d ",p->date);
		p=p->next;
	}
	printf("\n");
};

NODE* creat()//定义创建链表函数 
{
 	NODE *head,*p,*q;
	int x,cycle=1;
	head=(NODE*)malloc(sizeof(NODE));
	head->next=NULL;
	p=head;
	while(cycle)
	{
		printf("\n请输入一个整数用于建立单链表，当输入为0时建立结束：");
		scanf("%d",&x);
		if(x!=0)
		{
			q=(NODE*)malloc(sizeof(NODE));
			q->date=x;
			q->next=NULL;
			p->next=q;
			p=q;
		}
		else
		{
			cycle=0;
		}
	}
	return head;
};

void bubblesort(NODE *head)//定义冒泡排序算法 
{
	NODE *p,*q,*a;
	int Temp;
	for(a=head->next;a->next!=NULL;a=a->next)
	{
		p=head->next;
		q=p->next;
		Temp=p->date;
		while(q)
	{
		if(p->date>q->date)
		{
			Temp=p->date;
			p->date=q->date;
			q->date=Temp;
		}
		q=q->next;
		p=p->next;
	}
	}
};

void insert(NODE *head)//定义插入算法 
{
	int a;
	NODE *q;
	q=(NODE*)malloc(sizeof(NODE));
	printf("请输入你想插入的数值：");
	scanf("%d",&a);
	printf("\n");
	q->date=a;
	q->next=head->next;
	head->next=q;
};

void deletebyvalue(NODE *head)//定义按值删除节点算法 
{
	int a;
	NODE *p,*q;
	q=head;
	p=head->next;
	printf("请输入你想删除的数值：");
	scanf("%d",&a);
	printf("\n");
	while(p)
	{
		if(p->date==a)
		{
			q->next=p->next;
			p->next=NULL;
			free(p);
			break;
		}
		else if(p->next==NULL&&p->date!=a)
		{
			printf("该链表中没有你所输入的数值！");
		}
		p=p->next;
		q=q->next;
	}
};

void deletebyposition(NODE *head)//定义按位置删除节点算法 
{
	int a,b;
	NODE *p,*q;
	b=1; 
	q=head;
	p=head->next;
	printf("请输入你想删除的节点的位置(第一个节点的位置为1)：");
	scanf("%d",&a);
	printf("\n");
	while(p)
	{
		if(b==a)
		{
			q->next=p->next;
			p->next=NULL;
			free(p);
			break;
		}
		else if(p->next==NULL&&b!=a)
		{
			printf("该链表中没有你所输入的位置！");
		}
		p=p->next;
		q=q->next;
		b++;
	}
};

int main()
{
	int a;
	NODE *P;
	printf("请先建立单链表：\n");
	P=creat();
	printf("1是打印此单链表，2是对单链表的有序插入，3是由指定节点的值来删除此节点，4是由指定节点的位置来删除此节点.\n");
	printf("请输入你想要进行的操作：");
	scanf("%d",&a);
	if(a==1)
	{
		print(P);
	}
	else if(a==2)
	{
		insert(P);
		bubblesort(P);
		print(P);
	}
	else if(a==3)
	{
		deletebyvalue(P);
		print(P);
	}
	else if(a==4)
	{
		deletebyposition(P);
		print(P);
	}
	else
	{
		printf("你的输入有误！\n");
	}
	
	return 0;
}
