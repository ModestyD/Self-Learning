#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#define MAX 10 
void printqueue(int queue[],int front,int rear)//定义打印循环队列函数 
{
	int i;
	for(i=(front+1)%11;i!=rear;i=(i+1)%11)
	printf("%d ",queue[i]);
	printf("%d ",queue[i]);
	printf("\n"); 
};
 
int inqueue(int queue[],int front,int rear)//定义入循环队列函数 
{
	int x; 
	if(front==(rear+1)%MAX)
		printf("\n 循环队列已满！\n");
	else
	{
		printf("请输入入此循环队列的值：");
		fflush(stdin); 
		scanf("%d",&x);
		rear =(rear+1)%MAX;
		queue[rear]=x;
		printf("\n入队列后，此循环队列为：");
		printqueue(queue,front,rear);
	}

	return rear;
};

int outqueue(int queue[],int front,int rear)//定义出循环队列函数 
{
	if(front==rear)
		printf("\n此循环队列为已为空！\n");
	else
	{
		front=(front+1)%MAX;
		printf("\n出此循环队列的值为：%d",queue[front]);
		if(rear==front)
		{
			printf("\n此循环队列为空队列");
		}
		else
		{
			printf("\n出队列后，此循环队列为：");
			printqueue(queue,front,rear);
		} 
	}
	return front;
};

int length_queue(int queue[],int front,int rear )//定义计算循环队列长度函数 
{
	int i=(front+1)%MAX,j=0;
	while(i!=rear&&rear!=front)
	{
		j++;
		i=(i+1)%11;
	}
	if(rear==front)
		printf("此循环队列的长度为0\n");
	else
		printf("此循环队列的长度为%d\n",j+1);
};

void chose()//定义选择操作函数 
{
	printf("\n请选择对队列的操作，1为入队列，2为出队列，3为求队列长度:");
};

int main()
{
	int queue[MAX];
	int front=0,rear=0;
	int p,q;
	printf("\n请输入一组数组成队列（以空格隔开不同的数，以#结束,最多9个数）："); 
	while(scanf("%d",&p))
	{
		rear=(rear+1)%MAX;
		queue[rear]=p;
	}
	printf("\n队列为：");
 	printqueue(queue,front,rear);//打印初始化循环队列 
 	chose();
	fflush(stdin); 
 	while(scanf("%d",&q))
 	{
 		switch(q)
 		{
		 	case 1:rear=inqueue(queue,front,rear); 
			chose();
		 	break;
			case 2:front=outqueue(queue,front,rear); 
			chose();
			break;
			case 3:	length_queue(queue,front,rear); 
			chose();
			break;
		 }
	 }
	 return 0;
}