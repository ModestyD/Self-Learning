#include <stdio.h>
void insert(int a[],int length)//定义直接插入算法 
{
	int i,j,Temp;
	for(i=1;i<length;i++)
	{
		j=i-1;
		Temp=a[i];
		while(j>=0&&Temp<a[j])
		{
			a[j+1]=a[j];
			j--;
		}
		a[j+1]=Temp;
	}
};

void shellsort(int a[], int length)//定义希尔排序算法 
{
	int distance;  
    int p,q;  
    int temp;
    for(distance=length/2;distance>0;distance/=2)
    {
    	for(p=distance;p<length;p++)
    	{  
            temp = a[p];  
            for(q=p-distance;q>=0&&temp<a[q];q-=distance)  
            {  
                a[q+distance]=a[q];  
            }  
            a[q+distance]=temp;  
        }
    }
};

int main()
{
	int i,j,q;
	int a[]={5, 55, 9, 75, 44, 65};
	printf("未排序的数列是: \n");
	for(i=0;i<6;i++)  
    {  
        printf(" %d ",a[i]);  
    }
    printf("\n");
    printf("请选择你想要进行的排序方法，1是直接插入排序，2是希尔排序：");
    scanf("%d",&q);
    if(q==1)
    {
    	shellsort(a,6);
    	printf("排序后的数列是: \n");
    	for(j=0;j<6;j++)  
    	{  
       	 	printf(" %d ",a[j]);  
    	}
    	printf("\n");
    }
    else if(q==2)
    {
    	insert(a,6);
    	printf("排序后的数列是: \n");
    	for(j=0;j<6;j++)  
    	{  
       	 	printf(" %d ",a[j]);  
    	}
    	printf("\n");
    }
    else
    {
    	printf("你的输入错误！\n");
    }
	
	return 0;
}
