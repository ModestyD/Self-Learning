#include <stdio.h>
#include <malloc.h>
struct node//定义链表结构体 
{
	int date;
	struct node *next;
};
typedef struct node NODE;

void print(NODE *head)//定义打印函数 
{
	NODE *p;
	p=head->next;
	printf("该链表为：");
	while(p->date!=0)
	{
		printf(" %d ",p->date);
		p=p->next;
	}
	printf("\n");
};

NODE* creat()//定义创建链表函数 
{
 	NODE *head,*p,*q;
	int x,cycle=1;
	head=(NODE*)malloc(sizeof(NODE));
	head->next=NULL;
	p=head;
	while(cycle)
	{
		printf("\n请输入一个整数用于建立单链表，当输入为0时建立结束：");
		scanf("%d",&x);
		if(x!=0)
		{
			q=(NODE*)malloc(sizeof(NODE));
			q->date=x;
			q->next=NULL;
			p->next=q;
			p=q;
		}
		else
		{
			cycle=0;
		}
	}
	return head;
};

NODE* Inversion(NODE *head)//定义倒置函数 
{
	NODE*q,*p;
	q=(NODE*)malloc(sizeof(NODE));
	q->next=NULL;	
	while(head->next!=NULL){
		p=head->next;			
		head->next=p->next;
		p->next=q->next;
		q->next=p;
	}
	return q;
};

NODE* Combine(NODE *head1,NODE *head2)//定义结合函数 
{
	NODE*p,*q,*head,*s,*k;
	p=head1->next;
	q=head2->next;
	head=(NODE*)malloc(sizeof(NODE));
	head->next=NULL;
	k=head;
	while(q&&p)
	{
		if(p->date>q->date)
		{
			s=q;
			q=q->next;
			s->next=k->next;
			k->next=s;
			k=s;
		}
		else if(p->date==q->date)
		{
			s=p;
			p=s->next;
			s->next=k->next;
			k->next=s;
			k=s;
			s=q;
			q=s->next;
			s->next=k->next;
			k->next=s;
			k=s;
		}
		else
		{
			s=p;
			p=s->next;
			s->next=k->next;
			k->next=s;
			k=s;	
		}
	}		
		if(p==NULL)
		{
			while(q)
			{
			s=q;
			q=q->next;
			s->next=k->next;
			k->next=s;
			k=s; 	
			}
		}
		else if(q==NULL)
		{
			while(p)
			{
			s=p;
			p=p->next;
			s->next=k->next;
			k->next=s;
			k=s; 	
			}
			
		}		
	return head;
}
int main()
{
	NODE *p,*q,*c,*d;
	int a;
	printf("若你想进行链表的倒置请输入1，若你想进行链表的合并请输入2：");
	scanf("%d",&a);
	if(a==1)
	{
		p=creat();
		d=Inversion(p); 
		print(d);
	}
	else if(a==2)
	{
		printf("请创建第一个链表：\n");
		p=creat();
		printf("请创建第二个链表：\n");
		q=creat();
		c=Combine(p,q);
		print(c); 
	}
	else
	{
		printf("你的输入有误！\n");
	}
	return 0;
}