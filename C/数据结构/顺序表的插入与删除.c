#include <stdio.h>
void insert(int a[],int n,int k,int x)//定义插入算法 
{
	int i;
	if(k<1||k>n+1)
	{
		printf("插入失败！\n"); 
	} 
	else
	{
		n++;
		for(i=n;i>=k;i--)
		{
			a[i]=a[i-1];
		}
		a[k-1]=x;
	}
};

void drop(int a[],int n,int k)//定义删除算法 
{
	int i;
	if(k<1||k>n)
	{
		printf("删除错误！\n");
	}
	for(i=k;i<n;i++)
	{
		a[i-1]=a[i];
	}
	
};

int main()
{
	int a[]={1,2,3,4,5};
	printf("该有序表为:%d %d %d %d %d\n",a[0],a[1],a[2],a[3],a[4]);
	int n,k,x,i;
	n=5;
	printf("若你想插入请输入1，若你想删除请输入0：\n");
	scanf("%d",&i);
	if(i==1)
	{
		printf("请输入你想插入结点的位置与数值：");
		scanf("%d%d",&k,&x);
		insert(a,n,k,x);
		printf("该有序表插入后输出为：%d %d %d %d %d %d\n",a[0],a[1],a[2],a[3],a[4],a[5]);
	}
	else
	{
		printf("请输入你想删除结点的位置：");
		scanf("%d",&k);
		drop(a,n,k);
		printf("该有序表删除后输出为：%d %d %d %d \n",a[0],a[1],a[2],a[3]);
	}

	return 0;
}
