#include "stdafx.h"
#include "Array.h"


Array::Array(int length)
{
	if (0 == length)
	{
		m_head = NULL;
		m_length = 0;
	}
	else
	{
		m_head = new int[length];
		if (NULL != m_head)
		{
			m_length = length;
		}
		else
		{
			m_length = 0;
		}
	}
}

/*
Array::Array(const Array & a)
{
	m_length = a.m_length;
	m_head = NULL;

 	if (m_length > 0)
	{
		m_head = new int[m_length];
		if (NULL != m_head)
		{
			for (int i = 0; i < m_length; i++)
			{
				m_head[i] = a.m_head[i];
			}
		}
	}
}
*/

Array::~Array()
{
	if (NULL != m_head)
	{
		delete[] m_head;
	}
}

int & Array::operator[](int i)
{
	return m_head[i];
}

Array Array::operator+(const Array & A)
{
	int length = m_length < A.m_length ? m_length : A.m_length;
	Array temp(length);
	for (int i = 0; i < length; i++)
	{
		temp.m_head[i] = m_head[i] + A.m_head[i];
	}

	return temp;
}
