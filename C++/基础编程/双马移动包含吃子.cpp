#include<iostream>
#include<math.h>
#include<string>
using namespace std;

class horse
{
	public:
		horse(int x=0,int y=0,int z=0);
		void inithorse();
		void move();
	private:
		int h_x;
		int h_y;
		int num;
		friend class chessboard;
};

class chessboard
{
	public:
		chessboard();
		void whichkind();
		void refresh();
		void ifkill();
	private:
		int index;
		int chess_x;
		int chess_y;
		horse m_horse[4];
};

chessboard::chessboard()
{
	horse m_horse[4]={horse(1,9,1),horse(7,9,2),horse(1,0,3),horse(7,0,4)};
};

horse::horse(int x,int y,int z)
{
	h_x=x;
	h_y=y;
	num=z;
};

void chessboard::ifkill()
{
		if(index==1)
		{
			if(m_horse[0].h_x==m_horse[2].h_x&&m_horse[0].h_y==m_horse[2].h_y)
			{
			m_horse[2].h_x=-1;
			m_horse[2].h_y=-1;
			}
			else if(m_horse[0].h_x==m_horse[3].h_x&&m_horse[0].h_y==m_horse[3].h_y)
			{
			m_horse[3].h_x=-1;
			m_horse[3].h_y=-1;
			}
			else if(m_horse[0].h_x==m_horse[1].h_x&&m_horse[0].h_y==m_horse[1].h_y)
			{
				cout<<"你的输入不符合规则！将初始化棋子2！";
				cout<<endl;
			 	m_horse[1].h_x=7;
				m_horse[1].h_y=9;
			} 
		}
		else if(index==2)
		{
			if(m_horse[1].h_x==m_horse[2].h_x&&m_horse[1].h_y==m_horse[2].h_y)
			{
			m_horse[2].h_x=-1;
			m_horse[2].h_y=-1;
			}
			else if(m_horse[1].h_x==m_horse[3].h_x&&m_horse[1].h_y==m_horse[3].h_y)
			{
			m_horse[3].h_x=-1;
			m_horse[3].h_y=-1;
			}
			else if(m_horse[1].h_x==m_horse[0].h_x&&m_horse[1].h_y==m_horse[0].h_y)
			{
				cout<<"你的输入不符合规则！将初始化棋子1！";
				cout<<endl;
			 	m_horse[0].h_x=1;
				m_horse[0].h_y=9;
			}
		}
		else if(index==3)
		{
			if(m_horse[2].h_x==m_horse[0].h_x&&m_horse[2].h_y==m_horse[0].h_y)
			{
			m_horse[0].h_x=-1;
			m_horse[0].h_y=-1;
			}
			else if(m_horse[2].h_x==m_horse[1].h_x&&m_horse[2].h_y==m_horse[1].h_y)
			{
			m_horse[1].h_x=-1;
			m_horse[1].h_y=-1;
			}
			else if(m_horse[2].h_x==m_horse[3].h_x&&m_horse[2].h_y==m_horse[3].h_y)
			{
				cout<<"你的输入不符合规则！将初始化棋子4！";
				cout<<endl;
			 	m_horse[1].h_x=7;
				m_horse[1].h_y=0;
			}
		}
		else
		{
			if(m_horse[3].h_x==m_horse[0].h_x&&m_horse[3].h_y==m_horse[0].h_y)
			{
			m_horse[0].h_x=-1;
			m_horse[0].h_y=-1;
			}
			else if(m_horse[3].h_x==m_horse[1].h_x&&m_horse[3].h_y==m_horse[1].h_y)
			{
			m_horse[1].h_x=-1;
			m_horse[1].h_y=-1;
			}
			else if(m_horse[3].h_x==m_horse[2].h_x&&m_horse[3].h_y==m_horse[2].h_y)
			{
				cout<<"你的输入不符合规则！将初始化棋子3！";
				cout<<endl;
			 	m_horse[1].h_x=1;
				m_horse[1].h_y=0;
			}
		}
		
	
}

void chessboard::whichkind()
{	
	string name;
	cout<<"请输入将要移动的象棋名称：";
	cin>>name;
	cout<<endl;
	if(name=="horse")
	{
		m_horse[0].inithorse();
		m_horse[1].inithorse();
		m_horse[2].inithorse();
		m_horse[3].inithorse();
		while(1)
		{
		cout<<"左下边的棋子代号为1，右下边的棋子代号为2，左上边的棋子代号为3，右上边的棋子代号为4，请输入想要移动的棋子：";
		cin>>index;
		cout<<endl;
		if(index==1)
		{
			m_horse[0].move();
			ifkill();
			refresh();	
		}
		else if(index==2)
		{
			m_horse[1].move();
			ifkill();
			refresh();
		}
		else if(index=3)
		{
			m_horse[2].move();
			ifkill();
			refresh();
		}
		else if(index=4)
		{
			m_horse[3].move();
			ifkill();
			refresh();
		}
		else 
		{
			cout<<"您输入的代号错误！请重新输入";
			cout<<endl; 
		}
		}
	}
	else
	{
		while(name!="horse")
		{	
			cout<<"您输入的象棋种类还未创建！请重新输入：";
			cin>>name;
			cout<<endl;
		}
	}
};

void horse::inithorse()
{
	int a,b,c;
 	cout<<"请输入此马的坐标与标号(先输入横坐标再输入纵坐标再输入标号并用空格隔开)：";
 	cin>>a>>b>>c;
 	h_x=a;
 	h_y=b;
 	num=c;
	for(int a=0;a<=9;a++)
	{
		for(int b=0;b<=8;b++)
		{
			if(a==h_y&&b==h_x)
			{
				cout<<num;
			}
			else
			cout<<"0";
		}
	cout<<endl;
	}
};

void chessboard::refresh()
{
	for(int a=0;a<=9;a++)
	{
		for(int b=0;b<=8;b++)
			{
				if(a==m_horse[0].h_y&&b==m_horse[0].h_x)
				{
					cout<<"1";
				}
				else if(a==m_horse[1].h_y&&b==m_horse[1].h_x)
				{
					cout<<"2";
				}
				else if(a==m_horse[2].h_y&&b==m_horse[2].h_x)
				{
					cout<<"3";
				}
				else if(a==m_horse[3].h_y&&b==m_horse[3].h_x)
				{
					cout<<"4";
				}
				else 
				{
					cout<<"0";
				}
			}
		cout<<endl;
	}
}

void horse::move()
{	
	int r_x,r_y;
	cout<<"请输入想要移动的坐标(先横后纵并使用空格间隔)：";
	cin>>r_x>>r_y;
	cout<<endl;
	if(r_x>8||r_y>9||r_x<0||r_y<0)
	{
		cout<<"您输入的坐标超出了棋盘范围！";
	} 
	else if(abs(r_x-h_x)==1&&abs(r_y-h_y)==2||abs(r_x-h_x)==2&&abs(r_y-h_y)==1)
	{
		 h_x=r_x;
		 h_y=r_y;
	}
	else
	{
		cout<<"您输入的坐标不符合行子规范！";
		cout<<endl;
	}	
};

int main()
{	
	chessboard cb;
	for(int a=0;a<=9;a++)
	{
		for(int b=0;b<=8;b++)
		{		
			cout<<"0";
		}
	cout<<endl;
	}
	cb.whichkind();
	return 0;
}