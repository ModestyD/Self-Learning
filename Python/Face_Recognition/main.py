#coding=gbk
from tkinter import *
from PIL import Image, ImageTk
import face_matching_GUI
import Recognition_results_Interface

class PunchCard:
    def __init__(self):
        self.root = Tk()
        self.root.title('课堂考勤系统')

        sw = self.root.winfo_screenwidth()  # 得到屏幕宽度
        sh = self.root.winfo_screenheight()  # 得到屏幕高度
        # 设置窗口宽高为700，400
        ww = 700
        wh = 400
        x = (sw - ww) / 2
        y = (sh - wh) / 2
        self.root.geometry("%dx%d+%d+%d" % (ww, wh, x, y))  # 使窗口初始化时在屏幕中间
        self.root.resizable(False, False)  # 设置窗口不可拉伸
        self.body()

    def body(self):
        self.photo = ImageTk.PhotoImage(file='images/bg1.png')  # 导入背景图
        canvas = Canvas(self.root, width=720, height=420)  # 创建一个画布
        canvas.create_image(300, 200, image=self.photo)
        canvas.pack(expand='yes', fill='both')

        button1 = Button(canvas, text='打卡', font=('微软雅黑', 14, 'bold'), width=10, height=2,command=self.show_fmGUI)
        button1.pack(side='left', anchor='center', expand='yes', padx=20)

        button2 = Button(canvas, text='查看记录', font=('微软雅黑', 14, 'bold'), width=10, height=2,command=self.results_Interface)
        button2.pack(side='left', anchor='center', expand='yes', padx=20)

        button3 = Button(canvas, text='退出', font=('微软雅黑', 14, 'bold'), width=10, height=2, command=quit)
        button3.pack(side='right', anchor='center', expand='yes', padx=20)

    def show_fmGUI(self):
        face_matching_GUI.Window()

    def results_Interface(self):
        Recognition_results_Interface.Window()

    def quit(self):
        sys.exit()

if __name__ == "__main__":
	test= PunchCard()
	test.root.mainloop()