#encoding=utf-8
import tkinter
from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
import numpy as np
import pandas as pd
import MySQLdb
import xlrd
import connect_DB

class Window:
    filehandler1 = 0
    filehandler2 = 0
    def __init__(self):
        self.root = Toplevel()  # 初始化窗口对象
        self.root.title('刷脸打卡系统')
        self.root.geometry('1000x1000')  # 为窗口设置尺寸
        self.body()
        self.choice()

    def body(self):

        def go():
            connect_DB.connectDB()
            file = '记录表.xls'

            workbook = xlrd.open_workbook(file)
            # 根据sheet索引或者名称获取sheet内容
            sheet1 = workbook.sheet_by_index(0)  # sheet1索引从0开始
            sheet1 = workbook.sheet_by_name('sheet 1')

            rows = sheet1.row_values(0)
            treeview = ttk.Treeview(self.root, height=20,show="headings", columns=rows)#新建表格

            for i in range(0, len(rows)):
                treeview.heading(rows[i], text=rows[i])  # 显示表头

            #取出每列的值
            sno = sheet1.col_values(0)
            name=sheet1.col_values(1)
            cname= sheet1.col_values(2)
            wnum= sheet1.col_values(3)
            cnum=sheet1.col_values(4)
            record= sheet1.col_values(5)

            for i in range(1,sheet1.nrows):
                treeview.insert('', i, values=(sno[i],name[i],cname[i],wnum[i],cnum[i],record[i]))
            treeview.place(x=0,y=100)

        botton = Button(self.root, text='确定', command=go, font=('微软雅黑', 10, 'bold'),relief='raised', width=8, height=1)
        botton.grid(row=0,column=10)

    def choice(self):
        def go1(*args):  # 处理事件，*args表示可变参数
            self.filehandler1 = open("data1.txt", "w+")
            list_em = comboxlist1.get()
            self.filehandler1.write(list_em + "\n")
            #print(list_em)  # 打印课程的值
            self.filehandler1.close()

        def go2(*args):  # 处理事件，*args表示可变参数
            self.filehandler2 = open("data2.txt", "w+")
            list_em = comboxlist2.get()
            self.filehandler2.write(list_em + "\n")
            #print(list_em)  # 打印周的值
            self.filehandler2.close()

        def go3(*args):  # 处理事件，*args表示可变参数
            self.filehandler3 = open("data3.txt", "w+")
            list_em = comboxlist3.get()
            self.filehandler3.write(list_em + "\n")
            #print(list_em)  # 打印节数的值
            self.filehandler3.close()

        label1= Label(self.root, text="请选择课程:",font=('微软雅黑', 10, 'bold'))
        label1.grid(row=0, column=1)
        label2= Label(self.root, text="请选择周数:",font=('微软雅黑', 10, 'bold'))
        label2.grid(row=0, column=4)
        label3= Label(self.root, text="请选择节数:",font=('微软雅黑', 10, 'bold'))
        label3.grid(row=0, column=7)


        comvalue1 = tkinter.StringVar()  # 窗体自带的文本，新建一个值
        comvalue2 = tkinter.StringVar()  # 窗体自带的文本，新建一个值
        comvalue3 = tkinter.StringVar()  # 窗体自带的文本，新建一个值

        comboxlist1 = ttk.Combobox(self.root, textvariable=comvalue1)  # 初始化
        comboxlist1.grid(row=0,column=2)
        comboxlist2 = ttk.Combobox(self.root, textvariable=comvalue2)  # 初始化
        comboxlist2.grid(row=0, column=5)
        comboxlist3 = ttk.Combobox(self.root, textvariable=comvalue3)  # 初始化
        comboxlist3.grid(row=0, column=8)

        comboxlist1["values"] = ("deep learning", "专业综合实践", "Python")
        comboxlist2["values"] = (
            "第1周", "第2周", "第3周", "第4周", "第5周", "第6周", "第7周", "第8周", "第9周", "第10周", "第11周", "第12周", "第13周", "第14周",
            "第15周", "第16周")
        comboxlist3["values"] = (
            "第1节", "第2节", "第3节", "第4节", "第5节", "第6节", "第7节", "第8节", "第9节", "第10节", "第11节", "第12节")

        comboxlist1.bind("<<ComboboxSelected>>", go1)  # 绑定事件,(下拉列表框被选中时，绑定go()函数)
        comboxlist2.bind("<<ComboboxSelected>>", go2)  # 绑定事件,(下拉列表框被选中时，绑定go()函数)
        comboxlist3.bind("<<ComboboxSelected>>", go3)  # 绑定事件,(下拉列表框被选中时，绑定go()函数)




