#识别全班人脸——训练CNN模型
import cv2
import numpy as np
import os
import tensorflow as tf
import pandas as pd
from keras.utils import to_categorical
import math

#获取图片信息以及对应的标签
def get_img_label(imgpath,imgnum):
    imglist = os.listdir(imgpath)

    name = []
    label_list = []
    image_list = []

    #图片信息列表
    for imgname in imglist:
        images = cv2.imread(imgpath + '/' + imgname)
        image_list.append(images)

    for i in range(0,len(imglist),imgnum):
        people_name = imglist[i].split('_')[0]
        name.append(people_name)

    #标签信息列表
    for i in range(0,len(name)):
        for imgname in imglist:
            if imgname.split('_')[0] == name[i]:
                label = i
                label_list.append(label)
    #将图片信息与标签信息进行对应
    dataframe = {'img':image_list,'label':label_list}
    result = pd.DataFrame(dataframe)

    return result

#将训练集打乱
def sample_train(dataframe):
    new = dataframe.sample(frac=1)

    return new

#修改图片、标签的格式（便于传进网络）
def get_data(dataframe):
   #将图片信息列表转化为np.array格式
   img_list = dataframe['img'].values.tolist()
   img_list = np.array(img_list)

   # 将图片信息列表转化为np.array格式
   lab_list = dataframe['label'].values.tolist()
   lab_list = to_categorical(lab_list)   #将标签转化为one—hot形式
   lab_list = np.array(lab_list)

   return img_list,lab_list

#将训练集分为小批次
def next_batch():
    global batch_count
    global batch_size
    global n_batch
    global train_img
    global train_label
    global train_get

    if batch_count > n_batch:
        batch_count = 1
        train_get = sample_train(train_get)
        train_img,train_label = get_data(train_get)

    batch_img = train_img[(batch_count - 1) * batch_size:batch_count * batch_size]
    batch_label = train_label[(batch_count - 1) * batch_size:batch_count * batch_size]
    batch_count += 1

    return batch_img,batch_label

#获取训练集、测试集、验证集
trainpath = u'D:/Project/PycharmProject/Face_Recognition/train_img'
train_get = get_img_label(trainpath,7)
train_sample = sample_train(train_get)
train_img,train_label = get_data(train_sample)

testpath = u'D:/Project/PycharmProject/Face_Recognition/test_img'
test = get_img_label(testpath,2)
test_img,test_label = get_data(test)

validationpath = u'D:/Project/PycharmProject/Face_Recognition/validation_img'
validation = get_img_label(validationpath,1)
validation_img,validation_label = get_data(validation)

batch_count = 1
batch_size = 20
n_batch = math.ceil(len(train_img) / batch_size)


#CNN准备工作
#卷积层过滤器filter
def weight_variable(shape):
    initial = tf.truncated_normal(shape,stddev = 0.1)
    return tf.Variable(initial)

#卷积层偏置项
def bias_variable(shape):
    initial = tf.Variable(tf.constant(0.1,shape = shape))
    return initial

#卷积层
def conv2d(x,w,padding):
    return tf.nn.conv2d(x,w,strides = [1,1,1,1],padding=padding)#第几张图片，长，宽，深度

#池化层————最大池化
def max_pool_2x2(x):
    return tf.nn.max_pool(x,ksize=[1,2,2,1],strides=[1,2,2,1],padding='SAME')#ksize池化层过滤器大小，strides控制步长--第几张图片，长，宽，深度

#搭建神经网络
x = tf.placeholder(tf.float32,[None,60,60,3],name="x-input")
y = tf.placeholder(tf.float32,[None,33],name="y-output")

keep_drop = tf.placeholder(tf.float32)

#第一层卷积层和池化层
#卷积层：filter：5x5，通道数为1，全0填充，深度为6
#激活函数：relu
#最大池化：2x2，步长为2
w_conv1=weight_variable([3,3,3,32])
b_conv1=bias_variable([32])

h_conv1=tf.nn.bias_add(conv2d(x,w_conv1,'SAME'),b_conv1)
h_relu1=tf.nn.relu(h_conv1)
h_pool1=max_pool_2x2(h_relu1)

#第二层卷积层和池化层
#卷积层：filter：5x5，步长为1，不填充，深度为16
#激活函数：relu
#最大池化：2x2，步长为2
w_conv2=weight_variable([3,3,32,64])
b_conv2=bias_variable([64])

h_conv2=tf.nn.bias_add(conv2d(h_pool1,w_conv2,'SAME'),b_conv2)
h_relu2=tf.nn.relu(h_conv2)
h_pool2=max_pool_2x2(h_relu2)

w_conv3=weight_variable([3,3,64,128])
b_conv3=bias_variable([128])

h_conv3=tf.nn.bias_add(conv2d(h_pool2,w_conv3,'SAME'),b_conv3)
h_relu3=tf.nn.relu(h_conv3)
h_pool3=max_pool_2x2(h_relu3)

#第三层全连接层

#第一个全连接层
w_fc1=weight_variable([8*8*128,1000])
b_fc1=bias_variable([1000])
h_pool3_flat=tf.reshape(h_pool3,[-1,8*8*128])
h_fc1=tf.nn.relu(tf.matmul(h_pool3_flat,w_fc1)+b_fc1)
h_fc1_drop = tf.nn.dropout(h_fc1,keep_drop)


w_fc2=weight_variable([1000,500])
b_fc2=bias_variable([500])
h_fc2=tf.nn.relu(tf.matmul(h_fc1_drop,w_fc2)+b_fc2)
h_fc2_drop = tf.nn.dropout(h_fc2,keep_drop)

w_fc3=weight_variable([500,300])
b_fc3=bias_variable([300])
h_fc3=tf.nn.relu(tf.matmul(h_fc2_drop,w_fc3)+b_fc3)
h_fc3_drop = tf.nn.dropout(h_fc3,keep_drop)

w_fc4=weight_variable([300,33])
b_fc4=bias_variable([33])

output=tf.matmul(h_fc3_drop,w_fc4)+b_fc4
prediction=tf.nn.softmax(output)

cross_entropy=tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=output)
loss=tf.reduce_mean(cross_entropy)
optimizer=tf.train.AdamOptimizer(0.001)
train=optimizer.minimize(loss)

correct_prediction=tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
accuracy=tf.reduce_mean(tf.cast(correct_prediction,tf.float32))


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for epoch in range(100):
        for i in range(n_batch):
            batch_img,batch_label = next_batch()
            #print("第",i,":",batch_img,type(batch_img),batch_img.shape)
            sess.run(train,feed_dict={x:train_img,y:train_label,keep_drop:1.2})


        acc_vali=sess.run(accuracy,feed_dict={x:validation_img,y:validation_label,keep_drop:1.2})
        print("iter",epoch,",validation Accuracy:",acc_vali)
    pred = sess.run(prediction, feed_dict={x: test_img, y: test_label, keep_drop: 1.2})
    print("预测标签：", [np.argmax(one_hot)for one_hot in pred])
    print("真正标签:", [np.argmax(one_hot)for one_hot in test_label])
    test_acc=sess.run(accuracy,feed_dict={x:test_img,y:test_label,keep_drop:1.2})
    print("Testing Accuracy",test_acc)
