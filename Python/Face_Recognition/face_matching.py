#coding=gbk
from tkinter import *
from PIL import Image,ImageTk
from tkinter.messagebox import *
import dlib
import cv2
import numpy as np
import os

# 获取训练图片文件夹中的文件路径
file_path = "D:/Project/PycharmProject/Face_Recognition/train"
imglist = os.listdir(file_path)

# 获取训练集的特征点坐标集合
def face_recognition():
    # 人脸特征点检测
    predictor_path = "shape_predictor_68_face_landmarks.dat"
    detector = dlib.get_frontal_face_detector()  # 获取人脸分类器
    predictor = dlib.shape_predictor(predictor_path)  # 获取人脸检测器

    all_data = []  # 存储所有训练图片的128维向量

    for imgname in imglist:
        image = cv2.imread(file_path + '/' + imgname)

        dets = detector(image, 1)  # 人脸标定
        # enumerate是一个Python的内置方法，用于遍历索引
        # index是序号；face是dets中取出的dlib.rectangle类的对象，包含了人脸的区域等信息
        for index, face in enumerate(dets):
            points = []
            shape = predictor(image, face)  # 寻找人脸的68个特征点

            # 遍历所有特征点，取出坐标，存入列表中
            for index, pt in enumerate(shape.parts()):
                pt_pos = (pt.x, pt.y)
                points.append(list(pt_pos))
        all_data.append(points)
    return all_data

# 计算训练集与待检测的人脸的欧式距离
def get_distance(data_1, data_2):
    diff = 0
    diff_list = []
    for i in range(len(data_2)):
        for j in range(len(data_1)):
            dis = np.sqrt((data_1[j][0] - data_2[i][j][0]) ** 2  + (data_1[j][1] - data_2[i][j][1]) ** 2)
            diff += dis
        diff_list.append(diff)
        diff = 0
    print(diff_list)
    min_index = diff_list.index(min(diff_list))
    print(min_index)
    return min_index

def init_face_cascade():
    face_cascade = cv2.CascadeClassifier(r'haarcascade_frontalface_default.xml')
    return face_cascade

def video_loop(face_cascade, camera):
    global img, faces
    success, img = camera.read()  # 从摄像头读取照片
    if success:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            img_faces = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

        rgb_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        current_image = Image.fromarray(rgb_img)  # 将图像转换成Image对象
        imgtk = ImageTk.PhotoImage(image=current_image)
        return imgtk

def checkin():
    cv2.waitKey(50)
    if faces == ():
        showinfo('提示', '打卡失败！')
    else:
        for (x, y, w, h) in faces:
            img_select = img[y:y + h, x:x + w]
            resized_img = cv2.resize(img_select, (300, 300))

            # 人脸特征点检测
            predictor_path = "shape_predictor_68_face_landmarks.dat"
            detector = dlib.get_frontal_face_detector()  # 获取人脸分类器
            predictor = dlib.shape_predictor(predictor_path)  # 获取人脸检测器

            dets = detector(resized_img, 1)  # 人脸标定
            points_location = []    #获取所有特征点的坐标
            # enumerate是一个Python的内置方法，用于遍历索引
            # index是序号；face是dets中取出的dlib.rectangle类的对象，包含了人脸的区域等信息
            for index, face in enumerate(dets):
                shape = predictor(resized_img, face)  # 寻找人脸的68个特征点

                # 遍历所有点，并用蓝色的点表示出来
                for index, pt in enumerate(shape.parts()):
                    pt_pos = (pt.x, pt.y)
                    points_location.append(list(pt_pos))

                    cv2.circle(resized_img, pt_pos, 1, (255, 0, 0), -1)
                cv2.imshow(u'打卡界面', resized_img)
            showinfo('提示', 'XXX打卡成功！')

        all_data = face_recognition()
        difference = get_distance(points_location,all_data)

        img_path = imglist[difference]
        result_path = file_path + '/' + img_path
        result_img = cv2.imread(result_path)
        cv2.imshow(u'识别结果', result_img)
        cv2.waitKey(0)

