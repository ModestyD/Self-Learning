#coding=gbk
from tkinter import *
import cv2
import face_matching


class Window:
    def __init__(self):
        self.face_matching_GUI = Toplevel()  # 初始化窗口对象
        self.face_matching_GUI.title('刷脸打卡系统')
        self.face_matching_GUI.geometry('1200x1000')  # 为窗口设置尺寸
        self.face_cascade = face_matching.init_face_cascade()
        self.camera = cv2.VideoCapture(0)
        self.body() #为窗口内容组件初始化
        self.refresh_img()

    def body(self):
        self.panel_cream = Label(self.face_matching_GUI)  # 创建一个标签
        self.panel_cream.pack(side = TOP)

        face_matching_clockin = Button(self.face_matching_GUI, text='打卡', font=('微软雅黑', 14, 'bold'), command=self.check_in, height=2, width=15)
        face_matching_clockin.pack()

        face_sys_return = Button(self.face_matching_GUI, text='返回系统主界面', font=('微软雅黑', 14, 'bold'),command=self.shutdown, height=2, width=15)  # 创建一个按钮标签，初始化按钮对象
        face_sys_return.pack()  # 部署按钮标签


    def refresh_img(self):
        imgtk = face_matching.video_loop(self.face_cascade,self.camera)
        self.panel_cream.imgtk = imgtk
        self.panel_cream.config(image=imgtk)
        self.face_matching_GUI.after(50, self.refresh_img)

    def check_in(self):
        face_matching.checkin()

    def shutdown(self):
        self.face_matching_GUI.destroy()
