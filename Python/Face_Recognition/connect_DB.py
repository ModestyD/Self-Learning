#连接数据库
#coding=gbk
import pymysql as ps
import xlwt as xw
import codecs

def connectDB():
    #连接数据库，创建游标对象
    db=ps.connect('localhost','root','wangjin','studentinfo',charset='utf8')
    cursor = db.cursor()

    #打开文件，并读取其中的值（去掉末尾换行符）
    file1=open('data1.txt','r')
    c=file1.read()
    c_name=c.strip('\n')

    file2=open('data2.txt','r')
    w=file2.read()
    w_num=w.strip('\n')

    file3=open('data3.txt','r')
    clas=file3.read()
    c_num=clas.strip('\n')

    #查询数据库中的数据
    sqlselect="SELECT * FROM student_infomation WHERE class_name='%s' AND week_num='%s' AND class_num='%s'" % (c_name,w_num,c_num)

    #执行查询语句，得到结果
    cursor.execute(sqlselect)
    results = cursor.fetchall()

    wb=xw.Workbook(encoding='utf-8')         #创建一个excel工作簿，编码utf-8，表格中支持中文
    sheet=wb.add_sheet('sheet 1')            #创建一个sheet
    columnName = ['学号','姓名','课程名','周数','节数','是否打卡']  #定义所有的列名，共6列
    for i in range(len(columnName)):         #将列名插入表格，共6列
        sheet.write(0,i,columnName[i])       #对第0行，第i列，写入columnName[i]中的内容
    '''
    dateFormat = xw.XFStyle()                #通过XFStyle控制excel中的格式
    dateFormat.num_format_str = 'yyyy-mm-dd-hh:ss'
    '''
    rows = len(results)                      #获取行数
    for i in range(rows):
        for j in range(len(columnName)):
            sheet.write(i+1,j,results[i][j]) #写入表格（第0行为表头，从第1行开始写）
    wb.save('记录表.xls')                    #保存表格，并命名

    #关闭游标对象和数据库
    cursor.close()
    db.close()