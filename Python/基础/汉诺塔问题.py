def hannuota():
    def move(a, b, c):
        print('move', a, 'to', c)
    def hanoi(a, b, c, n):
        if n > 1:
            hanoi(a, c, b, n-1)
            move(a, b, c)
            hanoi(b, a, c, n-1)
        else:
            move(a, b, c)
    n = input('请输入A柱的圆盘数量:')
    hanoi('A', 'B', 'C', int(n))
print("开始运行汉诺塔问题")
hannuota()
print("移动完毕！")