#coding=gbk
import socket

#创建socket，指定IP地址类型和通信类型
s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

#服务器地址
saddr = ("127.0.0.1",8888)

#发送数据
s.sendto(b"hello",saddr)

#从服务器接受数据
data,_ = s.recvfrom(1024)
print("从服务器接受消息:{0}".format(data.decode()))

#释放资源
s.close()