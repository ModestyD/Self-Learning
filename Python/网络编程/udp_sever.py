#coding=gbk
import socket

#创建socket,指定IP地址类型和通信类型
s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

#绑定本机
s.bind(('',8888))
print("服务器启动")

#从客户端接受数据
data, caddr = s.recvfrom(1024)
print("从客户端接受的消息：{0}".format(data.decode()))

#给客户端发送数据
s.sendto("你好".encode(),caddr)

#释放资源
s.close()