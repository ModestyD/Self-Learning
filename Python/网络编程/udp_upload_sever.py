#coding=gbk
import socket

HOST = ''
PORT = 7777
a = 1

file_name = "test.jpg"

#创建socket,指定IP地址类型和通信类型
with socket.socket(socket.AF_INET,socket.SOCK_DGRAM) as s:

    s.bind((HOST,PORT))
    print("服务器启动")

    while True:
        buffer = []
        while True:
            data, caddr = s.recvfrom(1024)

            if data:
                buffer.append(data)
                print("第", a, "次接收成功！")
                a += 1
                if data == bytes("stop",encoding="utf8"):
                    print(data)
                    break

        print(type(buffer))
        b = bytes().join(buffer)

        with open(file_name, "wb") as f:
            f.write(b)

        print("服务器接收完成")


