#coding=gbk
import socket

#创建socket，指定IP地址类型和通信类型
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

#连接服务器端
s.connect(("127.0.0.1",8888))
print("连接建立")

#发送数据
s.send(b"hello")

#从服务器接受数据
data = s.recv(1024)
print("从服务器接受消息:{0}".format(data.decode()))

#释放资源
s.close()