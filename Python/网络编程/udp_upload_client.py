#coding=gbk
import socket

HOST = "127.0.0.1"
PORT = 7777
saddr = (HOST,PORT)
file_name = "sun.jpg"
a=1

with socket.socket(socket.AF_INET,socket.SOCK_DGRAM) as s:
    with open(file_name,"rb") as f:
        while True:
            b = f.read(1024)
            if b:
                s.sendto(b,saddr)
                print("第",a,"次上传成功！")
                a+=1
            else:
                s.sendto(bytes("stop",encoding="utf8"),saddr)
                break
    print("客户端上传数据完成")