from tkinter import *
from tkinter import ttk
import numpy as np
import pandas as pd
'''
def displayform():

    data=pd.read_excel("签到表.xls")
    root = Toplevel()  # 初始框的声明
    columns = ("姓名", "签到")
    treeview = ttk.Treeview(root, height=18, show="headings", columns=columns)  # 表格
    treeview.column("姓名", width=300, anchor='center')  # 表示列,不显示
    treeview.column("签到", width=300, anchor='center')
    treeview.heading("姓名", text="姓名")  # 显示表头
    treeview.heading("签到", text="签到")
    treeview.pack(side=LEFT, fill=BOTH)
    name = ['代港', '王琴', '董建鑫']
    ipcode = ['是', '是', '是']
    for i in range(min(len(name), len(ipcode))):  # 写入数据
        treeview.insert('', i, values=(name[i], ipcode[i]))
    root.mainloop()  # 进入消息循环
displayform()
'''
class Window:
    def __init__(self):
        self.root = Toplevel()  # 初始化窗口对象
        self.root.title('刷脸打卡系统')
        self.root.geometry('600x450')  # 为窗口设置尺寸
        self.body()

    def body(self):
        data = pd.read_excel("签到表.xls")
        columns = ("姓名", "签到")
        treeview = ttk.Treeview(self.root, height=18, show="headings", columns=columns)  # 表格
        treeview.column("姓名", width=300, anchor='center')  # 表示列,不显示
        treeview.column("签到", width=300, anchor='center')
        treeview.heading("姓名", text="姓名")  # 显示表头
        treeview.heading("签到", text="签到")
        treeview.pack(side=LEFT, fill=BOTH)
        name = ['代港', '王琴', '董建鑫']
        ipcode = ['是', '是', '是']
        for i in range(min(len(name), len(ipcode))):  # 写入数据
            treeview.insert('', i, values=(name[i], ipcode[i]))
