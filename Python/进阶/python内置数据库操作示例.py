# coding=gbk
import sqlite3
conn = sqlite3.connect("user.db")   #连接到sqlite数据库，如果数据文件不存在，会自动创建

cursor = conn.cursor()  #创建一个游标对象

cursor.execute("create table user(id int(10) primary key, name varchar(20))")   #执行一条sql语句，创建数据表

cursor.close()  #关闭游标

conn.close()    #关闭连接

cursor.execute("insert into user (id,name) value ('0001','zs')")    #执行一条SQL语句，享数据表新增数据
cursor.execute("insert into user (id,name) value ('0002','李四')")

cursor.execute("select * from user")    #执行查询语句

result1 = cursor.fetchone() #获取一条查询信息
print(result1)

result12 = cursor.fetchall()    #获取所有查询信息
print(result12)

cursor.execute("update user set name = ? where id = ?", ("张三",1))   #修改数据，问号作为占位符替代具体操作，然后使用元祖替代问号

cursor.execute("delete from user where id = ?",(2,))    #删除数据

conn.commit()   #提交事务，保存修改信息