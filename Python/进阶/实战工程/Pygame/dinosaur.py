#coding=gbk
import random
import sys
import pygame
from  pygame.locals import *

background_image_filename = "img/bg.jpg"
dinosaur_image_filename = "img/03.png"
obstacles_imags_filename = "img/02.png"
width = 822
height = 260
FPS = 5


#地图类
class Mymap():

    #初始化加载地图
    def __init__(self,x,y):
        self.bg = pygame.image.load(background_image_filename).convert_alpha()
        self.x = x
        self.y = y

    #地图滚动显示
    def map_rolling(self):
        if self.x < -1024:
            self.x = 1024
        else:
            self.x -= 10

    #更新地图
    def map_update(self):
        screen.blit(self.bg,(self.x,self.y))

#恐龙类
class Dinosaur():
    #初始化
    def __init__(self):
        self.dinosaur_img = pygame.image.load(dinosaur_image_filename).convert_alpha()

        self.jumpState = False
        self.jumpHeight = 10
        self.jumpLow = 120
        self.jumpValue = 0
        self.y_init_speed = -25
        self.y_speed_Acceleration = 2

        #坐标
        self.x = 50
        self.y = self.jumpLow

    #定义跳跃标记
    def jump(self):
        self.jumpState = True

    #定义跳跃规则
    def jump_rule(self):
        if self.jumpState:
            if self.y >= self.jumpLow:
                self.jumpValue = self.y_init_speed + self.y_speed_Acceleration
                # print(self.y,'1',pygame.time.get_ticks)
            if self.y <= self.jumpLow:
                self.jumpValue = self.jumpValue + self.y_speed_Acceleration
                # print(self.y,'2',pygame.time.get_ticks)
            self.y += self.jumpValue

            if self.y >= self.jumpLow:
                self.jumpState = False

    #绘出恐龙
    def draw_dinosaur(self):
        screen.blit(self.dinosaur_img,(self.x,self.y))

class Obstacles():
    #初始化
    def __init__(self, x):
        self.obstacle = pygame.image.load(obstacles_imags_filename).convert_alpha()
        self.x = x
        self.y = 165

    def obstacles_rolling(self):
        if self.x < -52:
            self.x = random.randint(600,700)
        else:
            self.x -= 10

    def obstacles_update(self):
        screen.blit(self.obstacle,(self.x,self.y))

def mainGame():
    global screen, clock
    #是否游戏结束
    game_over = False
    #初始化pygame,为使用硬件做准备
    pygame.init()
    #初始化时钟
    clock = pygame.time.Clock()
    #创建了一个窗口
    screen = pygame.display.set_mode((width,height),0,32)
    #设置窗口标题
    pygame.display.set_caption("the game of dinosaur")

    #实例化两张地图
    map1 = Mymap(0,0)
    map2 = Mymap(1024,0)

    #实例化恐龙
    dinosaur = Dinosaur()

    #实例化障碍物
    obstacle = Obstacles(random.randint(600,700))

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                if dinosaur.y == dinosaur.jumpLow:
                    dinosaur.jump()
        if game_over == False:
            map1.map_rolling()
            map1.map_update()
            map2.map_rolling()
            map2.map_update()
            obstacle.obstacles_rolling()
            obstacle.obstacles_update()
            dinosaur.jump_rule()
            dinosaur.draw_dinosaur()
        if obstacle.x >= dinosaur.x:
            if (abs(obstacle.x) - dinosaur.x) < (dinosaur.x + 55) and (abs(obstacle.y) - dinosaur.y) < (dinosaur.y + 57):
                print("here")
            # sys.exit()
        # 刷新一下画面
        print("dinosaur.x=",dinosaur.x,"dinosaur.y=",dinosaur.y,"obstacle.x=",obstacle.x,"obstacle.y=",obstacle.y)
        pygame.display.flip()
        clock.tick(FPS)

if __name__ == '__main__':
    mainGame()