import sys
import pygame

#初始化并设置屏幕大小
pygame.init()
screen = pygame.display.set_mode((640,480))

ball = pygame.image.load('img/ball.jpg')
ballrect = ball.get_rect()
speed = [5,5]
clock = pygame.time.Clock()

while True:
    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    ballrect = ballrect.move(speed)
    if ballrect.left < 0 or ballrect.right > 640:
        speed[0] = -speed[0]
    if ballrect.top < 0 or ballrect.bottom > 480:
        speed[1] = -speed[1]
    screen.fill((255,255,255))
    screen.blit(ball,ballrect)
    pygame.display.flip()

pygame.quit()