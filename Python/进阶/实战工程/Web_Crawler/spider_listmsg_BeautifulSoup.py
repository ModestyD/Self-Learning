import requests
from bs4 import BeautifulSoup

def get_search_list(keyword=None,page=1):
    url = 'http://www.mailiangwang.com/'
    payload = {'keyword':keyword, 'pageid':page}

    #获取网页
    response = requests.get(url)
    print(response.url)
    print(response.status_code)

    #解析网页
    soup = BeautifulSoup(response.text, 'lxml')

    #链接
    names = soup.select('html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n1')

    #商家性质
    n2s = soup.select('html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n2')
    n3s = soup.select('html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n3')
    n4s = soup.select('html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n4')
    n5s = soup.select('html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n5')
    n6s = soup.select(
        'html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n6')
    n7s = soup.select(
        'html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n7')
    n8s = soup.select(
        'html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n8')
    n9s = soup.select(
        'html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n9')
    n10s = soup.select(
        'html body div.wrap div.mainbar.priceTab div#fuPrice.tabcon div.p_dataList.futuresList div#fuCornPrice.tabcon div.p_dataItem span.n10')
    with open('data_list_msg.txt','w') as f:
        f.write('名称|涨幅跌|最新价|昨收|今开|最高|最低|买入|卖出|成交量\n')
        for name,n2,n3,n4,n5,n6,n7,n8,n9,n10 in zip(names,n2s,n3s,n4s,n5s,n6s,n7s,n8s,n9s,n10s):
            print(name)
            name = name.text
            n2 = n2.text
            n3 = n3.text
            n4 = n4.text
            n5 = n5.text
            n6 = n6.text
            n7 = n7.text
            n8 = n8.text
            n9 = n9.text
            n10 = n10.text

            data = [name,n2,n3,n4,n5,n6,n7,n8,n9,n10,'\n']
            # f.write('|'.join(data))
            print("写入一行")
if __name__ == '__main__':
    get_search_list()