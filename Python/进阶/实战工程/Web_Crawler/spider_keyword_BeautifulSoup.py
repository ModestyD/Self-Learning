import requests
from bs4 import BeautifulSoup

def get_search_list(keyword=None,page=1):
    url = 'http://www.mailiangwang.com/biz/list'
    payload = {'keyword':keyword, 'pageid':page}

    #获取网页
    response = requests.get(url, params=payload)
    print(response.url)
    print(response.status_code)

    #解析网页
    soup = BeautifulSoup(response.text, 'lxml')

    #链接
    links = soup.select('html body div.wrap div.merchantList div.p_dataList div.p_dataItem span.n1 a')

    #商家性质
    captions = soup.select('html body div.wrap div.merchantList div.p_dataList div.p_dataItem span.n3')
    addrs = soup.select('html body div.wrap div.merchantList div.p_dataList div.p_dataItem span.n5')
    categories = soup.select('html body div.wrap div.merchantList div.p_dataList div.p_dataItem span.n6')

    with open('data_searchlist_msg.txt','w') as f:
        f.write('公司|注册资产|地址|产品\n')
        for link,caption,addr,category in zip(links,captions,addrs,categories):
            print(link.get('title'))
            name = link.get('title').strip()
            caption = caption.text
            addr = addr.text
            category = category.text

            data = [name,caption,addr,category,'\n']
            # f.write('|'.join(data))
            print("写入一行")
if __name__ == '__main__':
    get_search_list(u'大米',1)