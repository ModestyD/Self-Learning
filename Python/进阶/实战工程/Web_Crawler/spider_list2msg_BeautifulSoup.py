import requests
from bs4 import BeautifulSoup

def get_search_list(keyword=None,page=1):
    url = 'http://www.mailiangwang.com/'
    payload = {'keyword':keyword, 'pageid':page}

    #获取网页
    response = requests.get(url)
    print(response.url)
    print(response.status_code)

    #解析网页
    soup = BeautifulSoup(response.text, 'lxml')

    #链接
    names = soup.select('html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n1')
    print(names)
    #商家性质
    n2s = soup.select('html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n2')
    n3s = soup.select('html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n3')
    n4s = soup.select('html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n4')
    n5s = soup.select('html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n5')
    n6s = soup.select(
        'html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n6')
    n7s = soup.select(
        'html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n7')
    n8s = soup.select(
        'html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n8')
    n9s = soup.select(
        'html body div.wrap div#cornBlock.iblock div.main div.tabarea div#corn_supply.p_dataList.supplyList.tabcon div.p_dataItem div.hover span.n9')
    with open('data_list2_msg.txt','w') as f:
        f.write('商家名称|商品品类|产地|供货地|等级|年份|价格类型|参考单价(元/吨)|供货时间\n')
        for name,n2,n3,n4,n5,n6,n7,n8,n9 in zip(names,n2s,n3s,n4s,n5s,n6s,n7s,n8s,n9s):
            print(name.get('title'))
            name = name.get('title').strip()
            n2 = n2.get('title').strip()
            n3 = n3.text
            n4 = n4.text
            n5 = n5.text
            n6 = n6.text
            n7 = n7.text
            n8 = n8.text
            n9 = n9.text

            data = [name,n2,n3,n4,n5,n6,n7,n8,n9,'\n']
            f.write('|'.join(data))
            print("写入一行")
if __name__ == '__main__':
    get_search_list()