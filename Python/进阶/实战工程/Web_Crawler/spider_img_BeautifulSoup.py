import requests
from bs4 import BeautifulSoup
import urllib.request

img_num = 0

def download_jpg(url):
    global img_num
    response = requests.get(url)
    print(response.status_code)

    soup = BeautifulSoup(response.text, 'lxml')
    image_urls = soup.find_all('img', 'BDE_Image')
    print(type(image_urls))
    print(image_urls)

    for url in image_urls:
        image_url = url.get('src')
        #下载图片
        urllib.request.urlretrieve(image_url,"imgs\img{0:02d}.jpg".format(img_num))
        img_num += 1
        print(type(url))
        print(image_url)

def get_all_jpg(url,pages):
    for page in range(1,pages+1):
        new_url = url+"?pn="+str(page)
        download_jpg(new_url)

if __name__ == "__main__":
    get_all_jpg("https://tieba.baidu.com/p/5440445824",5)