
import urllib.request
import re

def download_jpg(url):
    response = urllib.request.urlopen(url)
    html = response.read().decode('utf-8')

    #输入判断正则表达式获取网页中的图片地址
    pattern = 'src="(https://imgsa\.baidu\.com/.+?\.jpg)"'
    image_urls = re.findall(pattern,html)

    #循环遍历下载图片
    i = 0
    for url in image_urls:
        urllib.request.urlretrieve(url,"imgs\img{0:02d}.jpg".format(i))
        i += 1
        print(url)

    print(response.getcode())

if __name__ == "__main__":
    download_jpg("https://tieba.baidu.com/p/6034507634")