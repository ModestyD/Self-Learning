#coding=gbk
import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import  Button
import os,warnings,time
from threading import Thread

#忽略warning信息
warnings.filterwarnings("ignore")

coor = []
ax = None

#获取手机显示图像
def get_screen_image():
    os.system('adb shell screencap -p /sdcard/daigang/screen.png')
    os.system('adb pull /sdcard/daigang/screen.png X:\Study\python-poject\wechatgame\screen_png')
    img = cv2.cvtColor(cv2.imread('X:\Study\python-poject\wechatgame\screen_png\screen.png'),cv2.COLOR_BGR2RGB)
    return img

def onClick(event):
    print('xdata=',event.xdata)
    print('ydata=',event.ydata)
    if event.xdata !=None and event.ydata != None:
        x = event.xdata
        y = event.ydata

        #x和y坐标是按钮控件自己的坐标系计算的，所以两个按钮的坐标不会太大
        if x >60 and y > 60 :
            #存放起点和终点坐标
            coor.append([x,y])
            if len(coor) == 1:
                print("选中起点")
            else:
                print("选中终点")

            #画出选中点
            global ax
            ax = figure.add_subplot(1,1,1)
            ax.plot(x,y,'ro')
            figure.canvas.draw()

            #跳到终点
            if len(coor) == 2:
                jumpto(coor.pop(),coor.pop())
                ax.lines.clear()
                #建立线程进行更新
                thread = Thread(target = update)
                thread.start()

def auto(event):
    if event.xdata !=None and event.ydata != None:
        x = event.xdata
        y = event.ydata

def jumpto(point1, point2):
    x1,y1 = point1
    x2,y2 = point2
    #计算起点和终点间的像素距离
    distance = ((x1-x2)**2 + (y1-y2)**2)**0.5
    #执行触摸手机屏幕命令，时间由距离乘以2.1决定
    os.system('adb shell input swipe 160 1300 160 1300 {0}'.format(int(distance*(1.05))))
    print("跳")

def resel_btn_click(event):
    print("重选")
    global ax
    if ax != None:
        coor.clear()
        ax.lines.clear()
        figure.canvas.draw()

def update():
    time.sleep(1)
    print("更新")
    axes_image.set_array(get_screen_image())
    figure.canvas.draw()

if __name__  == "__main__":
    figure = plt.figure()

    #截取手机屏幕并显示
    axes_image = plt.imshow(get_screen_image())

    #单击事件
    figure.canvas.mpl_connect("button_press_event",onClick)
    # figure.canvas.mpl_connect("button_press_event",auto)

    #重选按钮
    reselect_botton_positon = plt.axes([0.79,0.8,0.15,0.08])
    img_resel = cv2.cvtColor(cv2.imread('image/bt_reselect.png'),cv2.COLOR_BGR2RGB)
    reselect_button = Button(reselect_botton_positon, label="", image=img_resel)
    #绑定按钮与重选事件
    reselect_button.on_clicked(resel_btn_click)

    #自动按钮
    auto_button_position = plt.axes([0.79,0.6,0.15,0.08])
    img_auto = cv2.cvtColor(cv2.imread('image/bt_auto.png'),cv2.COLOR_BGR2RGB)
    auto_button = Button(auto_button_position, label="", image=img_auto)
    #绑定按钮与自动事件
    auto_button.on_clicked(auto)

    # 显示屏幕截图
    plt.show()

