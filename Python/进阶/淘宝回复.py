#coding=utf-8
def find_answer(question):
    with open('reply.txt','r') as f:
        while 1:
            line = f.readlines()
            if not line:
                break
            keyword = line.split('|')[0]
            reply = line.split('|')[1]
            if keyword in question:
                return reply
        return False