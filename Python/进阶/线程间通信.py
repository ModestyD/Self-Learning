#coding=gbk
from threading import Thread

#线程与进程资源共享制度不同，线程之间可调用，进程资源独立
balance = 200

def draw():
    print("---取钱开始---")
    global balance
    balance -= 100
    print("余额",balance,id(balance),sep=",")
    print("---取钱结束---")

def deposit():
    print("---存钱开始---")
    global balance
    balance += 100
    print("余额", balance, id(balance), sep=",")
    print("---存钱结束---")

if __name__ == '__main__':
    print("主进程开始")
    print("当前存款:",balance)
    p1 = Thread(target=draw,args=())
    p2 = Thread(target=deposit,args=())
    p1.start()
    p2.start()
    p1.join()
    p2.join()
    print("主进程结束")