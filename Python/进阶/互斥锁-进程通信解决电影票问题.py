#coding=gbk
from threading import Thread,Lock
import time
remainder = 20

def zhangsan():
    print("---张三订票开始---")
    global remainder
    # mutex.acquire()
    temp = remainder

    print("（zs）目前还有"+str(temp)+"张票")
    temp -= 1
    time.sleep(2)

    remainder = temp
    print("(zs)余票",remainder)
    # mutex.release()
    print("---张三订票结束---")

def lisi():
    print("---李四订票开始---")
    global remainder
    # mutex.acquire()
    temp = remainder

    print("(ls)目前还有"+str(temp)+"张票")
    temp -=1
    time.sleep(2)

    remainder = temp
    print("(ls)余票:",remainder)
    # mutex.release()
    print("---李四订票结束---")

if __name__ == '__main__':
    print("---主线程开始---")
    mutex = Lock()
    print("当前余票：",remainder)
    thread1 = Thread(target=zhangsan)
    thread2 = Thread(target=lisi)
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    print("---主线程结束---")
    print("当前余票:",remainder)



