#coding=gbk
from multiprocessing import Process,Queue
import multiprocessing

balance = 200

def draw(q,num):
    print("---取钱开始---")
    num.value = 10.12
    print(num.value,id(num))
    #global balance
    if not q.empty():
        balance = q.get()
        balance -= 100
        q.put(balance)
        print("余额",balance,id(balance),sep=",")
    print("---取钱结束---")

def deposit(q):
    print("---存钱开始---")
    # global balance
    if not q.empty():
        balance = q.get()
        balance += 100
        q.put(balance)
        print("余额",balance,id(balance),sep=",")
    print("---存钱结束---")

if __name__ == '__main__':
    print("主进程开始")
    num = multiprocessing.Value("d",10.0)
    print(num.value,id(num))
    print("当前存款:",balance)
    q = Queue(2)
    q.put(balance)
    p1 = Process(target=draw,args=(q,num))
    p2 = Process(target=deposit,args=(q,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
    print(num.value, id(num))
    print("主进程结束")