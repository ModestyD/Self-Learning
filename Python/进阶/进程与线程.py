#coding=gbk
from multiprocessing import Process,Pool
import time
def test(interval):
    print("我是子进程")

    # for i in range(interval):
    #     print(i)

    time.sleep(interval)
    print("子进程结束")

def main():
    print("我是主进程")
    p = Process(target = test,args=(10,),name="test")
    p.start()
    print("p.is_alive=",p.is_alive())
    print("p.name=", p.name)
    print("p.pid=",p.pid)
    print("等待子进程")
    p.join(2)    #等待子进程结束以后向下执行（默认不带参数的用法），当带参数时，等待参数s后执行
    print("p.is_alive=",p.is_alive())
    print("主进程结束")

if __name__ == '__main__':
    main()