#coding=gbk
from multiprocessing import Process,Pool
import os
import time
def test(interval):
    print("我是子进程：{:d},执行任务:{:d}".format(os.getpid(),interval))
    time.sleep(5)
    print("子进程结束：",os.getpgid())

def main():
    print("我是主进程：",os.getpid())
    p =  Pool(3)    #最大进程数为3的进程池
    for i in range (10):
        p.apply_async(func = test, args = {i,})#非阻塞方式调用test
    print("等待子进程")
    p.close()
    p.join()
    print("主进程结束")

if __name__ == '__main__':
    main()