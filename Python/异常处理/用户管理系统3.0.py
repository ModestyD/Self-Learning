import sys

class usersystem(object):#定义用户类
    count = 0
    def __init__(self):
        self.username = ''
        self.password = ''
        usersystem.count += 1
    def __del__(self):
        usersystem.count -= 1
    def printuser(self):
        for i in range(usersystem.count):
            print("username = ",self.username,end ="")
            print("password = ",self.password,end ="")

def creatobject():#实现创建一个用户对象方法
    new_user = usersystem()
    new_user.username = input("请输入新的用户名:")
    new_user.password = input("请输入新的密码:")
    return new_user

def creatobject2():
    new_user = usersystem()
    return new_user

def showsystem():#定义展示系统流程方法
    print("用户注册管理系统")
    print("     1.显示全部已注册用户")
    print("     2.查找/修改/删除用户信息")
    print("     3.添加新用户")
    print("     4.保存用户数据")
    print("     5.退出系统")

def showusers():#定义展示用户信息方法
    print("当前用户注册信息如下:", end='\n')
    for a in range(usersystem.count):
        print("username = ", user[a].username, end="\n")
        print("password = ", user[a].password, end="\n")
    print(end='\n')

def modifyusers():#定义修改用户信息方法
    find_username = input("请输入要查找的用户名:")
    for a in range(usersystem.count):
        if (user[a].username == find_username):
            print(user[a].username, "已注册！", end='\n')
            print("请选择操作:")
            print("     1.修改用户")
            print("     2.删除用户")
            key = input("请输入序号选择对应功能:")
            if (key == '1'):
                user[a].username = input("请输入新的用户名:")
                user[a].password = input("请输入新的密码:")
                print("已成功修改用户!")
                print(end='\n')
            elif (key == '2'):
                del user[a]
                print("已成功删除用户!")
                print(end='\n')
                break
            else:
                print("输入非法!")
                break
        elif (a == usersystem.count - 1):
            print(find_username, "不存在!")

def savedata():
    f_username = open('students_username.txt', 'w+')
    f_password = open('students_password.txt','w+')
    for a in user:
        f_username.write(a.username)
        f_username.write(',')
        f_password.write(a.password)
        f_password.write(',')
    f_username.close()
    f_password.close()


def delusers():#定义删除系统缓存数据并退出系统方法
    print("系统退出中！")
    number = usersystem.count
    for a in user:
        print(number)
        number -= 1
        del a
    print("系统退出完成!")

def importdata():
    f_username = open('students_username.txt', 'r+')
    f_password = open('students_password.txt', 'r+')
    username = f_username.read()
    username_list = username.split(',')
    password = f_password.read()
    password_list = password.split(',')
    A = creatobject2()
    key = len(username_list)
    while(key>0):
        key = key -1
        A.username = username_list[-key]
        A.password = password_list[-key]
        user.append(A)

f_error = open("errordata.txt","w+")
stderr = sys.stderr
sys.stderr = f_error
user = []
keyword = int(1)
print("欢迎使用该用户注册管理系统(warning:您操作所有数据若不进行保存将在退出系统时自动销毁)")
print("是否需要导入文件中数据?")
keyword2 = int(input("执行导入操作请输入1，否请输入2:"))
if keyword2 == 1:
    importdata()
    print("文件导入成功")
elif keyword2 == 2:
    pass
else:
    print("非法输入!")
while(keyword != 0):
    try:
        showsystem()
        key1 = input("请根据所需功能输入序号选择对应菜单:")
        if (key1 == '1'):
            showusers()
        elif (key1 == '2'):
            modifyusers()
        elif (key1 == '3'):
            a = creatobject()
            user.append(a)
            print("已成功添加用户!")
            print(end='\n')
        elif (key1 == '4'):
            print("正在保存缓存数据......")
            savedata()
            print(end='\n')
            break
        elif (key1 == '5'):
            delusers()
            break
        else:
            print("输入非法！请重新输入")
            print(end='\n')
    except:
        print("系统出错，正在记录错误......(错误已记录在错误日志文件中！)")
        print("请重新启动系统!")
        break
f_error.close()
sys.stderr = stderr