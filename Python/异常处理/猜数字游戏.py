import random
key_num = random.randint(0,100)
while True:
    try:
        user_key = input("请输入你心中所猜测的数字：")
        key = user_key.isdigit()
        if key ==False:
            raise IOError
        else:
            user_key = int(user_key)
    except IOError:
        print("您的输入为非整数！")
    else:
        if user_key > key_num:
            print("你猜测的数字大了")
        elif user_key < key_num:
            print("你猜测的数字小了")
        elif user_key == key_num:
            print("恭喜你！猜测正确！")
            break