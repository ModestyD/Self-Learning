while True:
    try:
        filname = input('请输入文件路径名或文件的名字:')
        f1 = open(filname.strip())
        print(f1.read())
    except FileNotFoundError:
        print("输入的文件未找到!")
    except:
        print("文件无法正常读出!")
    else:
        break
    finally:
        f1.close()

