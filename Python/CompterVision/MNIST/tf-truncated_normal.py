import tensorflow as tf
#初始化产生1值的数据
a = tf.Variable(tf.ones([2,3],dtype=tf.float32))
#初始化产生截断的正态分布数据
b = tf.Variable(tf.truncated_normal([3,2],mean=0,stddev=0.05,seed=2))
init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    print(sess.run(a))
    print(sess.run(b))
