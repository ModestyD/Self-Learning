#coding=gbk
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt
import numpy as np

#载入mnist数据
mnist = input_data.read_data_sets("./MNIST_data",one_hot=True)

#设置训练每批次大小
batch_size = 100
#设置训练批次数
n_batch = mnist.train.num_examples //batch_size
#命名空间
with tf.name_scope('input'):
    #构建神经网络
    x = tf.placeholder(tf.float32,[None,784],name="input_placeholder")
    y = tf.placeholder(tf.float32,[None,10],name="out_placeholder")
    keep_prob = tf.placeholder(tf.float32,name="prob_placeholder")
    lr = tf.Variable(0.001,dtype=tf.float32)

with tf.name_scope('layer1'):
    Weight1 = tf.Variable(tf.truncated_normal(shape=[784,1000],mean=0,stddev=0.15))
    biases1 = tf.Variable(tf.constant(0.1,shape=[1,1000]))
    #设置激活函数为双曲正切曲线
    L1 = tf.nn.tanh(tf.matmul(x,Weight1)+biases1)
    L1_drop = tf.nn.dropout(L1,keep_prob)

with tf.name_scope('layer2'):
    Weight2 = tf.Variable(tf.truncated_normal(shape=[1000,600],mean=0,stddev=0.15))
    biases2 = tf.Variable(tf.constant(0.1,shape=[1,600]))
    L2 = tf.nn.tanh(tf.matmul(L1_drop,Weight2)+biases2)
    L2_drop = tf.nn.dropout(L2,keep_prob)

with tf.name_scope('output'):
    Weight3 = tf.Variable(tf.truncated_normal([600,10],mean=0,stddev=0.15))
    biases3 = tf.Variable(tf.constant(0.1,shape=[1,10]))
    # output3 = tf.matmul(L2_drop,Weight3)+biases3
    prediction = tf.nn.softmax(tf.matmul(L2_drop,Weight3)+biases3)

with tf.name_scope('loss'):
    #定义损失函数为交叉熵代价函数
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=prediction))

with tf.name_scope('train'):
    #梯度下降
    # optimizer = tf.train.GradientDescentOptimizer(0.5)
    # train = optimizer.minimize(loss)
    train_step = tf.train.AdamOptimizer(lr).minimize(loss)

with tf.name_scope('acc'):
    #求准确率
    correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(prediction,1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    writer = tf.summary.FileWriter('logs/', sess.graph)
    for epoch in range(40):
        #?
        sess.run(tf.assign(lr,0.001*(0.95** epoch)))
        for batch in range(n_batch):
            batch_xs,batch_ys = mnist.train.next_batch(batch_size)
            sess.run(train_step,feed_dict={x:batch_xs,y:batch_ys,keep_prob:1.0})

        learning_rate = sess.run(lr)
        acc_vali = sess.run(accuracy,feed_dict={x:mnist.validation.images,y:mnist.validation.labels,keep_prob:1.0})
        print("iter:",str(epoch),",validation Accuracy:",str(acc_vali),"Learning_rate:",str(learning_rate))

    test_acc =  sess.run(accuracy,feed_dict={x:mnist.test.images,y:mnist.test.labels,keep_prob:1.0})
    print("Testing Acuracy:",test_acc)
