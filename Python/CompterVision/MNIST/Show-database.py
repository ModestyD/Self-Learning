#coding=gbk
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings('ignore')
#载入minst数据(当One_Hot为TURE时，取值为向量)
mnist = input_data.read_data_sets("./MNIST_data",one_hot=True)
print(mnist.train.num_examples)
print(mnist.validation.num_examples)
print(mnist.test.num_examples)

images = mnist.train.images
labels = mnist.train.labels
print(type(images[0]),images[0].shape)
print(images[0])
print(labels[0])

img = images[0].reshape(-1,28)
print(type(img),img.shape)
print(img)
plt.axis('off')
plt.imshow(img,cmap='gray')
plt.title(labels[0])
plt.show()

#显示多张图片
images,labels = mnist.train.next_batch(64)
plt.figure(figsize=(8,8))
for i in range (8):
    for j in range(8):
        img = images[i*8+j].reshape(-1,28)
        plt.subplot(8,8,i*8+j+1)
        plt.axis("off")
        plt.imshow(img,cmap='gray')
        plt.title(labels[i*8+j])
plt.show()

