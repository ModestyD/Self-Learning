#coding=gbk
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt
import numpy as np

#载入mnist数据
mnist = input_data.read_data_sets("./MNIST_data",one_hot=True)

#设置训练每批次大小
batch_size = 100
#设置训练批次数
n_batch = mnist.train.num_examples //batch_size

#构建神经网络
x = tf.placeholder(tf.float32,[None,784])
y = tf.placeholder(tf.float32,[None,10])
keep_prob = tf.placeholder(tf.float32)

Weight = tf.Variable(tf.truncated_normal([784,10],stddev=0.1))
biases = tf.Variable(tf.zeros([1,10]))
L1 = tf.nn.tanh(tf.matmul(x,Weight)+biases)


output = tf.matmul(x,Weight) + biases
prediction = tf.nn.softmax(output)

#损失函数
loss = tf.reduce_mean(tf.squared_difference(y,prediction))

#梯度下降
optimizer = tf.train.GradientDescentOptimizer(0.5)
train = optimizer.minimize(loss)

#求准确率
correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(prediction,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    for epoch in range(21):
        for batch in range(n_batch):
            batch_x,batch_y = mnist.train.next_batch(batch_size)
            sess.run(train,feed_dict={x:batch_x,y:batch_y})

        acc_vali = sess.run(accuracy,feed_dict={x:mnist.validation.images,y:mnist.validation.labels})
        print("iter:",epoch,",validation Accuracy:",acc_vali)

    test_acc =  sess.run(accuracy,feed_dict={x:mnist.test.images,y:mnist.test.labels})
    print("Testing Acuracy:",test_acc)
