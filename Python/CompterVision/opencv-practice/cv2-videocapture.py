# coding=gbk
import cv2
import numpy as np
videocapture = cv2.VideoCapture('test.mp4')
fps = videocapture.get(cv2.CAP_PROP_FPS)
width = int(videocapture.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(videocapture.get(cv2.CAP_PROP_FRAME_HEIGHT))
print(fps,width,height)
success,frame = videocapture.read()
while success and cv2.waitKey(5) == -1:
    cv2.imshow('main',frame)
    success,frame = videocapture.read()

cv2.destroyWindow('main')