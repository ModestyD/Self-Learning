# coding=gbk
import cv2
import numpy as np

face_cascade = cv2.CascadeClassifier(r'X:\Study\deeplearning\dataset\haarcascades\haarcascade_frontalface_default.xml')

videocapture = cv2.VideoCapture(0)

success,frame = videocapture.read()
count = 0
while success and cv2.waitKey(1000) == -1:
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        while count <=20:
            img = cv2.resize(gray[y:y+w,x:x+h],(200,200))
            picename = 'X:\Study\deeplearning\deeplearnertest\data\DKG_{0}.jpg'.format(count)
            count += 1
            cv2.imwrite(picename,img)

    cv2.imshow('face_matching', frame)
    success,frame = videocapture.read()

cv2.destroyWindow('face_matching')

videocapture.release()

cv2.waitKey()