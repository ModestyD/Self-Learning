#coding=gbk
import tensorflow as tf

#feed
input1 = tf.placeholder(tf.float32)
input2 = tf.placeholder(tf.float32)

output = tf.multiply(input1,input2)

with tf.Session() as sess:
    #feed的数据以字典的形式传入
    result = sess.run(output,feed_dict={input1:[2.0,1.0],input2:[3.0,4.0]})
    print(result)

#fetch

add = tf.add(input1,input2)

with tf.Session() as sess:
    result = sess.run([output,add],feed_dict={input1:[2.0,1.0],input2:[3.0,4.0]})
    print(result)

