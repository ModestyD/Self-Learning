#coding=gbk
import tensorflow as tf
import numpy as np

#以线性回归为例子简单介绍模型构建流程

#生成500个随机点
x_data = np.random.rand(500)
y_data = x_data*0.1+0.2


#构造一个线性模型
b = tf.Variable(0.)
k = tf.Variable(0.)
y = tf.add(tf.multiply(k,x_data),b)

#损失函数
loss = tf.reduce_mean(tf.squared_difference(y,y_data))  #均方误差

#梯度下降优化器
optimizer = tf.train.GradientDescentOptimizer(0.2)

#最小化损失函数
train = optimizer.minimize(loss)

#刷新变量值，即初始化变量（使初始张量流入声明节点）
init = tf.global_variables_initializer()

#创建session，进行会话
with tf.Session() as sess:
    sess.run(init)
    for step in range(500):
        sess.run(train)
        if step %20 == 0:
            print(step,sess.run([k,b,loss]))