#coding=gbk
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# 生成200随机样本
x_data = np.linspace(-0.5,0.5,200)[:,np.newaxis]
noise = np.random.normal(0,0.02,x_data.shape)
y_data = np.square(x_data)+noise

#d定义placeholder
x = tf.placeholder(tf.float32,[None,1])
y = tf.placeholder(tf.float32,[None,1])

#定义神经网络中间层
weights_L1 = tf.Variable(tf.random_normal([1,10]))
biases_L1 = tf.Variable(tf.zeros([1,10]))
z1 = tf.matmul(x,weights_L1) + biases_L1
a1 = tf.nn.tanh(z1)

#输出层
weights_L2 = tf.Variable(tf.random_normal([10,1]))
biases_L2 = tf.Variable(tf.zeros([1,1]))

z2 = tf.matmul(a1,weights_L2) + biases_L2
prediction = tf.nn.tanh(z2)

#损失函数
loss = tf.reduce_mean(tf.squared_difference(y,prediction))

#梯度下降优化
optimizer = tf.train.GradientDescentOptimizer(0.1)
train = optimizer.minimize(loss)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for _ in range(2000):
        sess.run(train,feed_dict={x:x_data,y:y_data})

    #获得预测图
    prediction_value = sess.run(prediction,feed_dict={x:x_data})

    #画图
    plt.scatter(x_data,y_data)
    plt.plot(x_data,prediction_value,'r-',lw=5)
    plt.show()