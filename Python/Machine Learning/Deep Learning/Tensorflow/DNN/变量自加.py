#coding=gbk
import tensorflow as tf

state = tf.Variable(0,name='counter')

new_value = tf.add(state,1)

update = tf.assign(state, new_value)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    print('state(init):',sess.run(state))
    for _ in range(5):
        print('new_value:',sess.run(new_value))
        print('update_state:',sess.run(update))