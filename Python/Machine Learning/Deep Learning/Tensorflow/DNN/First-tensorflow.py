#coding=gbk
import tensorflow as tf
    #图graph
    #会话session
    #张量tensor
    #节点op

m1 = tf.constant([[3,3]])   #设定常量
m2 = tf.constant([[2],[3]])

product = tf.matmul(m1,m2)  #设定过程

with tf.Session() as sess:  #创建会话
    result = sess.run(product)  #运行过程

x = tf.Variable([1,2])  #设定变量       -节点
a = tf.constant([3,3])  #设定常量       -节点

sub = tf.subtract(x,a)
add = tf.add(x,sub)

init = tf.global_variables_initializer()    #初始化变量  -节点

with tf.Session() as sess:
    sess.run(init)
    print(sess.run(sub))
    print(sess.run(add))

print(result)