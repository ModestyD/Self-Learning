#在网上摘录一段英文文本(尽量长一些)，粘贴到input.txt，
#统计其中每个单词的词频(出现的次数)，并按照词频的顺序写入out.txt文件，
#每一行的内容为“单词:频次”
import re
f1 = open('input.txt','r')
word_count = {}
f1_string =''
key_word = re.compile(r'\b[a-zA-Z]+\b')
for line in f1:
     f1_string += line
print(f1_string)
words = key_word.findall(f1_string)
print(words)
for word in words:
    if word in word_count:
        word_count[word] += 1
    else:
        word_count[word] = 1
print(word_count)
word_sort = sorted(word_count.items(),key=lambda item:item[1],reverse=True)
print(word_sort)
f2 = open('output.txt','r+')
for words in word_sort:
    words_list = list(words)
    words_list_new = [str(x) for x in words_list]
    f2.write(':'.join(words_list_new))
    f2.write("\n")
f1.close()
f2.close()
