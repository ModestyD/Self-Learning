import re
class DanCiBen(object):
    def __init__(self,my_dict={}):
        self.my_dict=my_dict

    def memorize(self,word,meaning):
        self.my_dict[word]=meaning

    def search(self,word):
        if word in self.my_dict.keys():
            return self.my_dict[word]
        else:
            return "这个单词还没有背！"

    def modify(self,word,meaning):
        self.my_dict[word]=meaning

    def get_hot_words(self):
        txt_list=['./cet/2010.06.txt','./cet/2010.12.txt',
                  './cet/2011.06.txt','./cet/2011.12.txt',
                  './cet/2012.06.txt','./cet/2012.12. txt',
                  './cet/2013.06.txt','./cet/2013.12.txt',
                  './cet/201006.txt','./cet/201012.txt',
                  './cet/201106.txt','./cet/201112.txt',
                  './cet/201206.txt','./cet/201212.txt',
                  './cet/201306.txt','./cet/201312.txt',
                  './cet/2014.06.txt','./cet/201406.txt',]
        #将所有的文本存入一个字符串
        full_txt_str=""

        for txt_name in txt_list:
            infile=open(txt_name,'r')
            for line in infile:
                full_txt_str+=line
        #转为小写
        full_txt_str=full_txt_str.lower()

        #将所有单词装入一个list
        word_list=re.findall(r'\b[a-zA-Z]+\b',full_txt_str)

        #统计词频
        word_count={}
        for word in word_list:
            if not word_count.has_key(word):
                word_count[word]=word_list.count(word)

        #排序
        sorted_word_count=sorted(word_count.items(),key=lambda item:item[1],reverse=True)
        return sorted_word_count

#类测试
dcb=DanCiBen()
#查单词
print ("hello:"+dcb.search('hello'))
#背单词
dcb.memorize('hello','你好')
dcb.memorize('world','单词')
#查单词
print ("hello:"+dcb.search('hello'))
print ("world:"+dcb.search('world'))
#修改单词含义
dcb.modify('world','世界')
#查单词
print ("world:"+dcb.search('world'))
#词频排序
dcb.get_hot_words()
